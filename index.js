// Array

var sayHello = 'Salam, Dunia!';

console.log(sayHello); // Salam, Dunia!

var hobbies = ['desain grafis', 'sinema', 'anime', 'light novel', 'sepak bola'];

console.log(hobbies); // [ 'desain grafis', 'sinema', 'anime', 'light novel', 'sepak bola' ]
console.log(hobbies.length); // 5

console.log(hobbies[0]); // desain grafis
console.log(hobbies[3]); // light novel

// Mengakses elemen terakhir dari array
console.log(hobbies[hobbies.length -1]); // sepak bola

// Metode Array

var feeling = ['dag', 'dig'];

feeling.push('dug'); // Menambahkan nilai "dug" ke index paling belakang
feeling.pop() // Menghapus nilai pada elemen terakhir array

console.log(feeling); // [ 'dag', 'dig', 'dug' ] 👉 [ 'dag', 'dig' ]

// .push()
var numbers = [0, 1, 2]

numbers.push(3)

console.log(numbers) // [ 0, 1, 2, 3 ]

numbers.push(4, 5)

console.log(numbers) // [ 0, 1, 2, 3, 4, 5 ]]

// .pop()
var numbers = [0, 1, 2, 3, 4, 5]

numbers.pop()

console.log(numbers) // [ 0, 1, 2, 3, 4 ]

// .unshift()
var numbers = [0, 1, 2, 3]

numbers.unshift(-1)

console.log(numbers) // [ -1, 0, 1, 2, 3 ]

// .shift()
var numbers = [0, 1, 2, 3]

numbers.shift()

console.log(numbers) // [ 1, 2, 3 ]

// .sort()
var animals = ['kera', 'gajah', 'musang']

animals.sort()

console.log(animals) // [ 'gajah', 'kera', 'musang' ]

// .slice()
var angka = [0, 1, 2, 3]
var irisan1 = angka.slice(1, 3)

console.log(irisan1) // [ 1, 2 ]

var irisan2 = angka.slice(0, 2)

console.log(irisan2) // [ 0, 1 ]

// Parameter kedua tidak diisi = sampai ke indeks terakhir
var angka = [0, 1, 2, 3]
var irisan3 = angka.slice(2)

console.log(irisan3) // [ 2, 3 ]

// "Shallow copy" dengan metode slice
var angka = [0, 1, 2, 3]
var salinAngka = angka.slice()

console.log(salinAngka) // [ 0, 1, 2, 3 ]

// .splice()
var fruits = ['banana', 'orange', 'grape']

fruits.splice(1, 0, 'watermelon')

console.log(fruits) // [ 'banana', 'watermelon', 'orange', 'grape' ]

var fruits = ['banana', 'orange', 'grape']

fruits.splice(0, 2)

console.log(fruits) // [ 'grape' ]

// .split() dan .join()
var biodata = 'name:john,doe'
var name = biodata.split(':')

console.log(name) // [ 'name', 'john,doe' ]

var title = ['pengalamanku', 'sebagai', 'seorang', 'pemrogram']
var slug = title.join('-')

console.log(slug) // pengalamanku-sebagai-seorang-pemrogram

// Sesi live~
// .reverse()
var arr = [1, 3, 4]
var newArr = arr.reverse()

console.log(newArr) // [ 4, 3, 1 ]

// Function

// Contoh 1
function tampilkan() {
  console.log('Salam!')
}

tampilkan() // Salam!

// Contoh 2
function munculkanAngkaDua() {
  return 2
}

var wadah = munculkanAngkaDua()

console.log(wadah) // 2

// Contoh 3
function kalikanDua(angka) {
  return angka * 2
}

var wadah = kalikanDua(2)

console.log(wadah) // 4

// Contoh 4
function jumlahkanAngka(angkaPertama, angkaKedua) {
  return angkaPertama + angkaKedua
}

console.log(jumlahkanAngka(5, 3)) // 8

// Contoh 5
function tampilkanAngka(angka = 1) {
  return angka
}

console.log(tampilkanAngka(5)) // 5
console.log(tampilkanAngka()) // 1

// Anonymous Function
var fungsiPerkalian = function(angkaPertama, angkaKedua) {
  return angkaPertama * angkaKedua
}

console.log(fungsiPerkalian(2, 4)) // 8

// Object

var personArr = ['John', 'Doe', 'male', 30]
var personObj = {
  firstName: 'John',
  lastName: 'Doe',
  gender: 'male',
  age: 30,
}

console.log(personArr[0]) // John
console.log(personObj.firstName) // John

// Deklarasi Object
var car = {
  brand: 'Ferrari',
  type: 'Sports Car',
  price: 50000000,
  'horse power': 986,
}

console.log(car)
// {
//   brand: 'Ferrari',
//   type: 'Sports Car',
//   price: 50000000,
//   'horse power': 986
// }

var car2 = {}

car2.brand = 'Lamborghini'
car2.type = 'Sports Car'
car2.price = 100000000
car2['horse power'] = 730

console.log(car2)
// {
//   brand: 'Lamborghini',
//   type: 'Sports Car',
//   price: 100000000,
//   'horse power': 730
// }

// Mengakses Nilai pada Object
var motorcycle1 = {
  brand: 'Honda',
  type: 'CUB',
  price: 1000,
}

console.log(motorcycle1.brand) // Honda
console.log(motorcycle1.type) // CUB

console.log(motorcycle1['price']) // 1000

// Array, secara teknis, adalah sebuah object
var array = [1, 2, 3]

console.log(typeof array) // object

// Array of Object

var mobil = [{merek: 'BMW', warna: 'merah', tipe: 'sedan'}, {merek: 'Toyota', warna: 'hitam', tipe: 'boks'}, {merek: 'Audi', warna: 'biru', tipe: 'sedan'}]

// .forEach()
var mobil = [{merek: 'BMW', warna: 'merah', tipe: 'sedan'}, {merek: 'Toyota', warna: 'hitam', tipe: 'boks'}, {merek: 'Audi', warna: 'biru', tipe: 'sedan'}]

mobil.forEach(function(item) {
  console.log('warna: ' + item.warna)
})
// warna: merah
// warna: hitam
// warna: biru

// .map()
var arrayWarna = mobil.map(function(item) {
  return item.warna
})

console.log(arrayWarna) // [ 'merah', 'hitam', 'biru' ]

// Sesi live~
// .map()
var arrayWarnaObj = mobil.map(function(item) {
  return {warna: item.warna}
})

console.log(arrayWarnaObj) // [ { warna: 'merah' }, { warna: 'hitam' }, { warna: 'biru' } ]

// .filter()
var arrayMobilFilter = mobil.filter(function(item) {
  return item.tipe != 'sedan'
})

console.log(arrayMobilFilter) // [ { merek: 'Toyota', warna: 'hitam', tipe: 'boks' } ]
